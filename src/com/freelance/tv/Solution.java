package com.freelance.tv;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class Solution {
    private static final int THREE_NUM_SIZE = 3;
    private static final int FIVE_NUM_SIZE = 5;
    private static Stack<Integer> s1 = new Stack<>();
    private static Stack<Integer> s2 = new Stack<>();

    public static void main(String[] args) {
        JFrame rootFrame = new JFrame();
        rootFrame.setSize(600, 400);
        rootFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel rootPanel = new JPanel();
        placeComponents(rootPanel);
        rootFrame.add(rootPanel);
        rootFrame.setVisible(true);
    }

    private static void placeComponents(JPanel panel) {
        panel.setLayout(null);

        JLabel label1 = new JLabel("Add 3 numbers in stack 1: ");
        label1.setBounds(20,20,190,25);
        panel.add(label1);

        JTextField txtThreeNums = new JTextField(20);
        txtThreeNums.setBounds(210,20,150,25);
        panel.add(txtThreeNums);

        JLabel label2 = new JLabel("Add 5 numbers in stack 2: ");
        label2.setBounds(20,80,190,25);
        panel.add(label2);

        JTextField txtFiveNums = new JTextField(20);
        txtFiveNums.setBounds(210,80,150,25);
        panel.add(txtFiveNums);


        JLabel stack1Before = new JLabel("Stack 1 before [0, 1, 2]");
        stack1Before.setBounds(20, 140, 600, 25);
        panel.add(stack1Before);

        JLabel stack2Before = new JLabel("Stack 2 before [0, 1, 2, 3, 5]");
        stack2Before.setBounds(20, 160, 600, 25);
        panel.add(stack2Before);

        JLabel stack1After = new JLabel("Stack 1 after [0, 1, 2]");
        stack1After.setBounds(20, 190, 600, 25);
        panel.add(stack1After);

        JLabel stack2After = new JLabel("Stack 2 after [0, 1, 2, 3, 5]");
        stack2After.setBounds(20, 210, 600, 25);
        panel.add(stack2After);

        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(370,20,100,25);
        panel.add(submitButton);

        submitButton.addActionListener(actionEvent -> {
            int[] threeNumsArr = TextFieldUtil.toArray(txtThreeNums.getText(), THREE_NUM_SIZE);
            int[] fiveNumsArr = TextFieldUtil.toArray(txtFiveNums.getText(), FIVE_NUM_SIZE);

            for (int i = 0; i < threeNumsArr.length; i++) {
                s1.push(threeNumsArr[i]);
            }

            stack1Before.setText("Stack 1 before: " + String.valueOf(s1));

            for (int i = 0; i < fiveNumsArr.length; i++) {
                s2.push(fiveNumsArr[i]);
            }

            stack2Before.setText("Stack 2 before: " + String.valueOf(s2));


            Iterator<Integer> stack1Iterator = s1.iterator();
            Iterator<Integer> stack2Iterator = s2.iterator();
            List<Integer> list1 = new ArrayList<>();
            List<Integer> list2 = new ArrayList<>();

            while (stack1Iterator.hasNext()) {
                list1.add(stack1Iterator.next());
            }

            while (stack2Iterator.hasNext()) {
                list2.add(stack2Iterator.next());
            }

            s1.clear();

            for (int i = 0; i < list2.size(); i++) {
                s1.push(list2.get(i));
            }

            s2.clear();

            for (int i = 0; i < list1.size(); i++) {
                s2.push(list1.get(i));
            }

            stack1After.setText("Stack 1 after: " + String.valueOf(s1));
            stack2After.setText("Stack 2 after: " + String.valueOf(s2));
        });


    }

}