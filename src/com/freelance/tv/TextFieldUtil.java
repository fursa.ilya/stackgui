package com.freelance.tv;

public class TextFieldUtil {

    public static int[] toArray(String input, int size) {
        String[] inputArr = input.split(" ");
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = Integer.parseInt(inputArr[i]);
        }

        return arr;
    }
}
